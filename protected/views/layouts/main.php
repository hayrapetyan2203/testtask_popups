<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="en">
 	<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print">
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection">
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/site.css">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/popup.css">
	<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="container" id="page">

	<div id="header" class="d-flex p-1">

		<div class="d-flex">
	        <div>Visits count: <span id="visits-count-span"><?= CHtml::encode($this->getVisitsCount()); ?></span></div>
	        <button class="option-button">
	        	<?= CHtml::ajaxLink('Reset visits', Yii::app()->createUrl('site/ResetVisits'), array(
				    'type' => 'GET',
				    'success' => 'js:function(response) {
				        $("#visits-count-span").val(response);
				        confirmReload();
				    }'
				), array('id' => 'reset-visits')); ?>
	        </button> 
   		</div>

	    <div class="d-flex">
	        <div>Default seconds</div>
	        <input type="number" min="0" id="default-seconds-input" value="<?= CHtml::encode($this->getDefaultSeconds()); ?>">
	        <button class="option-button">
				<?= CHtml::ajaxLink('Save', Yii::app()->createUrl('site/SaveDefaultSeconds'), array(
				    'type' => 'POST',
				    'data' => 'js:{defaultSeconds: $("#default-seconds-input").val()}',
				    'success' => 'js:function(response) {
				        $("#default-seconds-input").val(response);
				        confirmReload();
				    }'
				), array('id' => 'default-seconds-save')); ?>
	        </button> 
	        <input type="hidden" id="initialSeconds" value="<?=$this->getInitialSeconds() ?>">
	        <div id="countdown"></div>
	    </div>
 
	</div><!-- header -->

	<div id="mainmenu">
		<?php $this->widget('zii.widgets.CMenu',array(
			'items'=>array(
				/*array('label'=>'Home', 'url'=>array('/site/index')),*/
				/*array('label'=>'Contact', 'url'=>array('/site/contact')),
				array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
				array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)*/
			),
		)); ?>
	</div><!-- mainmenu -->
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php echo $content; ?>

	<div class="clear"></div>

</div><!-- page -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/popup.js"></script>
</body>
</html>
