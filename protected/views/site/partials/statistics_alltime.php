<h2>All Time Statistics</h2>
<table border="1">
    <thead>
        <tr>
            <th>Popup Variant</th>
            <th>Clicks</th>
            <th>Impressions</th>
            <th>Date</th>
        </tr>
    </thead>
    <tbody>
        <?php if (!empty($allTimeStatistics)): ?>
            <?php foreach ($allTimeStatistics as $stat): ?>
                <tr>
                    <td><?php echo CHtml::encode($stat->popup_variant); ?></td>
                    <td><?php echo CHtml::encode($stat->clicks); ?></td>
                    <td><?php echo CHtml::encode($stat->impressions); ?></td>
                    <td><?php echo CHtml::encode($stat->date); ?></td>
                </tr>
            <?php endforeach; ?>
        <?php else: ?>
            <tr>
                <td colspan="4">No data available.</td>
            </tr>
        <?php endif; ?>
    </tbody>
</table>

<div class="pagination">
    <?php $this->widget('CLinkPager', array(
        'pages' => $pages,
    )); ?>
</div>