<!-- Canvas for the Chart -->
<canvas id="dailyStatisticsChart" width="400" height="200"></canvas>

<script>
// Prepare the data for the chart
var popupVariants = <?php echo json_encode($popupVariants); ?>;

$.each(popupVariants, function(index, value) {
  popupVariants[index] = 'popup variant ' + value;
});
;

var clicks = <?php echo json_encode($clicks); ?>;
var impressions = <?php echo json_encode($impressions); ?>;

// Create the chart
var ctx = document.getElementById('dailyStatisticsChart').getContext('2d');
var dailyStatisticsChart = new Chart(ctx, {
    type: 'bar', // You can change this to 'line', 'pie', 'doughnut', etc.
    data: {
        labels: popupVariants,
        datasets: [{
            label: 'Clicks',
            data: clicks,
            backgroundColor: 'rgba(75, 192, 192, 0.2)',
            borderColor: 'rgba(75, 192, 192, 1)',
            borderWidth: 1
        }, {
            label: 'Impressions',
            data: impressions,
            backgroundColor: 'rgba(153, 102, 255, 0.2)',
            borderColor: 'rgba(153, 102, 255, 1)',
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    }
});
</script>
