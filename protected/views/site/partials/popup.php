<script>
  $(document).ready(function() {
    $("#dialog").addClass("active"); // Show the popup initially
    // Close popup when close button or overlay is clicked
    $(".popup-close, .popup-overlay").click(function() {
        $("#dialog").removeClass("active");
    });
});

</script>
<div id="dialog" class="popup">
    <div class="popup-content">
        <div class="popup-close">&times;</div>
        <div class="popup-text">
            <span>Date:</span><span><?php echo date('d-m-Y'); ?></span>
        </div>
        <div class="popup-text">
            <span>Description:</span><span>I'm in a popup variant 1</span>
        </div>
    </div>
</div>