<?php
/* @var $this StatisticsController */
/* @var $dailyStatistics Statistics[] */
?>

<h1 class="mt-2">Today's Statistics</h1>

<table border="1">
    <thead>
        <tr>
            <th>Popup Variant</th>
            <th>Clicks</th>
            <th>Impressions</th>
            <th>Date</th>
        </tr>
    </thead>
    <tbody>
        <?php if (!empty($dailyStatistics)): ?>
            <?php 
            $popupVariants = [];
            $clicks = [];
            $impressions = [];
            foreach ($dailyStatistics as $stat): 
                $popupVariants[] = CHtml::encode($stat->popup_variant);
                $clicks[] = CHtml::encode($stat->clicks);
                $impressions[] = CHtml::encode($stat->impressions);
            ?>
                <tr>
                    <td><?php echo CHtml::encode($stat->popup_variant); ?></td>
                    <td><?php echo CHtml::encode($stat->clicks); ?></td>
                    <td><?php echo CHtml::encode($stat->impressions); ?></td>
                    <td><?php echo CHtml::encode($stat->date); ?></td>
                </tr>
            <?php endforeach; ?>
        <?php else: ?>
            <tr>
                <td colspan="4">No data available for today.</td>
            </tr>
        <?php endif; ?>
    </tbody>
</table>

<?=$this->renderPartial('partials/chart', 
array(
    'popupVariants' => $popupVariants,
    'clicks' => $clicks,
    'impressions' => $impressions,
)) ?>