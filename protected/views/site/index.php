<?php
/* @var $this SiteController */
$this->pageTitle=Yii::app()->name;
?>

<?=$this->renderPartial('partials//statistics_today', array('dailyStatistics' => $dailyStatistics)); ?>
<?=$this->renderPartial('partials/statistics_alltime', array('allTimeStatistics' => $allTimeStatistics, 'pages' => $pages));  ?>

<?= CHtml::ajaxLink('', Yii::app()->createUrl('site/popup'), 
    array('update' => '#popup-div'), 
    array('id' => 'view-popup')
);
?>
<div id="popup-div"></div>
<script>
    $.get('popup', function(data) {
        localStorage.setItem("popupVariant", data.popupVariant);
        setTimeout(function() {
            $('#popup-div').html(data.popupHtml);
            saveImpression();
        }, data.seconds * 1000); // Convert seconds to milliseconds
    });
</script>