<?php

Yii::import('application.controllers.BaseController');

class SiteController extends BaseController
{
	private $visitsCount;
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{	
		// Get current date in 'Y-m-d' format
        $currentDate = date('Y-m-d');

        // Fetch statistics for the current day
        $dailyStatistics = Statistics::model()->findAll(
            'DATE(date) = :currentDate',
            array(':currentDate' => $currentDate)
        );

        // Fetch all-time statistics
        // Set up pagination for all-time statistics
        $criteria = new CDbCriteria();
        $criteria->order = 'date ASC';

        $count = Statistics::model()->count($criteria);
        $pages = new CPagination($count);
        $pages->pageSize = 10; // Set the number of records per page
        $pages->applyLimit($criteria);

        $allTimeStatistics = Statistics::model()->findAll($criteria);

        
        
		// Increment visit count
        $this->incrementVisitCount();

		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
        $this->render('index', array(
            'dailyStatistics' 	=> $dailyStatistics,
            'allTimeStatistics' => $allTimeStatistics,
            'pages' 			=> $pages,
        ));
	}	

	/**
	 * Handles saving of impression events for a specific popup variant.
	 *
	 * Retrieves the popup variant from the POST request parameters. Uses
	 * the `findStatistics` method to find or create a Statistics model
	 * corresponding to the popup variant. Increments the 'impressions' attribute
	 * of the Statistics model and saves it to the database. Sends a JSON
	 * response indicating success.
	 *
	 */
	public function actionSaveImpression()
	{
		$popupVariant = Yii::app()->request->getPost('popupVariant') ?? Statistics::POPUP_DEFAULT;

		$statistics = $this->findStatistics($popupVariant);
		$statistics->impressions += 1;
		$statistics->save(false);

		// Return JSON response with conditional data
	    header('Content-Type: application/json');
	    echo json_encode('ok');

	    Yii::app()->end();
	}

	/**
	 * Increments the visit count in the visit record.
	 *
	 * Retrieves the visit record using the `findVisitRecord` method,
	 * increments the 'count' attribute, and saves the updated record
	 * to the database.
	 */
    private function incrementVisitCount() : void
    {
        $visitRecord = $this->findVisitRecord();
		$visitRecord->count++;

        // Save the updated or new record
        $visitRecord->save();
    }    

	/**
	 * Handles the display of popups based on a random probability.
	 *
	 * Retrieves the visit record using the `findVisitRecord` method.
	 * Generates a random number to determine which popup variant to render.
	 * Renders the corresponding popup HTML and determines the popup variant.
	 * Sends a JSON response containing the rendered popup HTML, popup variant,
	 * and the calculated seconds for displaying the popup.
	 *
	 * @throws CHttpException If the request method is not POST.
	 */
	public function actionPopup()
	{
	    $visitRecord = $this->findVisitRecord();
	    $popupHtml = '';

	    // Determine which popup to render based on probability
	    $randomNumber 	  = mt_rand(1, 100); // Generates a random number between 1 and 100
	    if ($randomNumber <= 70) {
	        // Render the first popup (70% probability)
	        $popupHtml	  = $this->renderPartial('partials/popup', array('data' => 'Ur-data'), true);
	        $popupVariant = Statistics::POPUP_1;
	    } else {
	        // Render the second popup (30% probability)
	        $popupHtml    = $this->renderPartial('partials/popup2', array('data' => 'Ur-data'), true);
	        $popupVariant = Statistics::POPUP_2;
	    }

	    // Return JSON response with conditional data
	    header('Content-Type: application/json');
	    echo json_encode(array(
	        'popupHtml'    => $popupHtml,
	        'popupVariant' => $popupVariant,
	        'seconds' 	   => $visitRecord->calculatePopupShowingSeconds()
	    ));
	    Yii::app()->end();
	}

	/**
	 * Resets the visit count to zero.
	 *
	 * Retrieves the visit record using the `findVisitRecord` method,
	 * sets the 'count' attribute to zero, and saves the updated record
	 * to the database. Sends a JSON response containing the updated count.
	 *
	 * @throws CHttpException If the request method is not POST.
	 */
	public function actionResetVisits()
	{
		$visitRecord = $this->findVisitRecord();
		$visitRecord->count = 0;
		$visitRecord->save();

		// Return JSON response with conditional data
	    header('Content-Type: application/json');
	    echo json_encode($visitRecord->count);
	    Yii::app()->end();
	}	
	
	/**
	 * Saves the default seconds for displaying a popup.
	 *
	 * Retrieves the default seconds value from the POST request parameters.
	 * Updates the 'default_seconds' attribute in the visit setting retrieved
	 * using the `findVisitSetting` method. Sends a JSON response containing
	 * the updated default seconds value if successfully saved.
	 *
	 * @throws CHttpException If the request method is not POST or if the defaultSeconds parameter is missing or empty.
	 */
	public function actionSaveDefaultSeconds()
	{
		$defaultSeconds = Yii::app()->request->getPost('defaultSeconds');
		if($defaultSeconds){
			$visitSetting = $this->findVisitSetting();
			$visitSetting->default_seconds = $defaultSeconds;
			$visitSetting->save();

			// Return JSON response with conditional data
		    header('Content-Type: application/json');
		    echo json_encode($visitSetting->default_seconds);
		}
	    Yii::app()->end();
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}