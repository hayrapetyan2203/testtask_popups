<?php

class BaseController extends CController
{
	/**
	 * Retrieves the number of visits recorded.
	 *
	 * This function fetches the count of visits recorded in the database
	 * by accessing the `count` property of the visit record found using
	 * the `findVisitRecord` method.
	 *
	 * @return int The number of visits recorded.
	 */
	public function getVisitsCount() : int
	{
	    return $this->findVisitRecord()->count;
	}

	/**
	 * Retrieves the default seconds configured for displaying a popup.
	 *
	 * This function fetches the default seconds for displaying a popup
	 * by invoking the `getDefaultSeconds` method on the visit record
	 * obtained using the `findVisitRecord` method.
	 *
	 * @return int The default seconds configured for popup display.
	 */
	public function getDefaultSeconds() : int
	{
		return $this->findVisitRecord()->getDefaultSeconds();
	}	

	/**
	 * Retrieves the initial seconds configured for displaying a popup.
	 *
	 * This method fetches the initial seconds for displaying a popup
	 * by invoking the `calculatePopupShowingSeconds` method on the visit record
	 * obtained using the `findVisitRecord` method.
	 *
	 * @return int The initial seconds configured for popup display.
	 */
	public function getInitialSeconds() : int
	{
        return $this->findVisitRecord()->calculatePopupShowingSeconds();
	}

	/**
	 * Finds or creates a visit record based on the user's IP address.
	 *
	 * This method retrieves the visit record from the database using the IP address
	 * obtained from the current request. If no record is found, it creates a new
	 * visit record and sets the IP address before returning it.
	 *
	 * @return Visits The visit record found or created.
	 */
    public function findVisitRecord() : Visits
    {
    	$ipAddress = Yii::app()->request->getUserHostAddress();

        // Check if there's already a record for this IP address
        $visitRecord = Visits::model()->findByAttributes(array('ip' => $ipAddress));

        if (!$visitRecord) {
            // Create a new record if it doesn't exist
            $visitRecord = new Visits();
            $visitRecord->ip = $ipAddress;
        }
	
        return $visitRecord;
    }

	/**
	 * Finds the visit setting for the type 'seconds'.
	 *
	 * @return VisitSetting The visit setting found.
	 */
    public function findVisitSetting() : VisitSetting
    {
        return VisitSetting::model()->findByAttributes(array('type' => VisitSetting::TYPE_SECONDS));
    }

    /**
	 * Finds or creates a Statistics model for a specific popup variant and current day.
	 *
	 * @param int $popupVariant The variant of the popup.
	 * @return Statistics The Statistics model found or created.
	 */
	public function findStatistics(int $popupVariant) : Statistics
	{
	    // Find the Statistics model for the current day
		$statistics = Statistics::model()->find(
		    'DATE(date) = :currentDate AND popup_variant = :popupVariant',
		    array(':currentDate' => date('Y-m-d'), ':popupVariant' => $popupVariant)
		);

	    // If not found, create a new one
	    if ($statistics === null) {
	        $statistics = new Statistics();
	    }

	    // Return the model
	    return $statistics;
	}
	
	/**
	 * Handles saving of click events for a specific popup variant.
	 *
	 * This action retrieves the popup variant from the request parameters. It then
	 * finds or creates a Statistics model for the specified variant, increments
	 * the click count, and saves the model. Finally, it returns a JSON response
	 * indicating successful completion.
	 */
	public function actionSaveClicks()
	{
		$popupVariant = Yii::app()->request->getPost('popupVariant') ?? Statistics::POPUP_DEFAULT;

		$statistics = $this->findStatistics($popupVariant);
		$statistics->clicks += 1;
		$statistics->save(false);

		// Return JSON response with conditional data
	    header('Content-Type: application/json');
	    echo json_encode('ok');

	    Yii::app()->end();
	}	
}