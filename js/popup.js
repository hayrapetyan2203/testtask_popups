$(document).ready(function() {
    // Close popup when close button or overlay is clicked
    $(".popup-close, .popup-overlay").click(function() {
        $("#dialog").removeClass("active");
    });

    // Save click event when any part of the document is clicked
    $(document).on('click', function() {
        saveClicks();
    });

    // Start countdown when the page loads
    var initialSeconds = parseInt($('#initialSeconds').val(), 10);
    startCountdown(initialSeconds);
});

// Show popup dialog when '#view-popup' element is clicked
$('#view-popup').click(function(){
     $("#dialog").addClass("active");
});

// AJAX function to save click event for a popup variant
function saveClicks() {
    $.post('SaveClicks', { popupVariant : localStorage.getItem('popupVariant') }, function(data) {
        // Optionally handle response data
    });
}

// AJAX function to save impression event for a popup variant
function saveImpression() {
    $.post('SaveImpression', { popupVariant : localStorage.getItem('popupVariant') }, function(data) {
        // Optionally handle response data
    });
}

// Display confirmation dialog and reload page if confirmed
function confirmReload() {
    if (confirm("Operation status: Success! Do you want to reload the page?") == true) {
        $("#dialog").hide(); // Hide the dialog
        window.location.reload(); // Reload the page
    } 
}

// Function to start countdown timer
function startCountdown(seconds) {
    var timer = setInterval(function() {
        seconds--;
        if (seconds < 0) {
            clearInterval(timer);
            $('#countdown').text('Countdown Complete!');
        } else {
            var minutes = Math.floor(seconds / 60);
            var remainingSeconds = seconds % 60;
            $('#countdown').text(remainingSeconds);
        }
    }, 1000); // Update every 1 second
}
